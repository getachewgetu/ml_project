pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                // Checkout your Git repository
                checkout scm
            }
        }

        stage('Setup Virtual Environment') {
            steps {
                // Create a Python virtual environment in the project workspace
                sh 'python3 -m venv venv'
            }
        }

        stage('Install Dependencies') {
            steps {
                // Activate the virtual environment and install dependencies
                sh '. venv/bin/activate && pip install -r requirements.txt'
            }
        }

        stage('Run Machine Learning Experiment') {
            steps {
                // Activate the virtual environment and execute your script
                sh '. venv/bin/activate && python experiment.py'
            }
        }
    }

    post {
        success {
            // Define actions to be taken on success
            echo "Machine learning experiment successful!"
        }
        failure {
            // Define actions to be taken on failure
            echo "Machine learning experiment failed!"
        }
    }
}
