import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score, recall_score
from sklearn.metrics import precision_score, make_scorer, roc_curve, auc, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
from xgboost import XGBClassifier
from lightgbm import LGBMClassifier
# removing warning
import warnings
warnings.filterwarnings('ignore')

# Step 1: Read the dataset
#data = pd.read_csv('/content/drive/MyDrive/rta_data_final.csv')
data = pd.read_csv('rta_data_final.csv')
# Step 2: Handle Missing Values
data = data.replace('Unknown', pd.NA)
data = data.dropna()

# # Step 3: Perform Clustering

# # Select the features for clustering
# cluster_features = data[['hour', 'minute', 'Day_of_week',
#                         'Age_band_of_driver', 'Sex_of_driver', 'Educational_level',
#                         'Driving_experience', 'Type_of_vehicle',
#                         'Service_year_of_vehicle', 'Area_accident_occured',
#                         'Lanes_or_Medians', 'Road_allignment', 'Types_of_Junction',
#                         'Light_conditions', 'Weather_conditions', 'Road_surface_type',
#                         'Road_surface_conditions', 'Type_of_collision',
#                         'Number_of_vehicles_involved',
#                         'Vehicle_movement', 'Pedestrian_movement', 'Cause_of_accident', 'Accident_severity']]

# # Convert non-numeric categorical features to numeric using LabelEncoder
# label_encoder = LabelEncoder()
# for feature in cluster_features.columns:
#     if cluster_features[feature].dtype == 'object':
#         cluster_features[feature] = label_encoder.fit_transform(cluster_features[feature].astype(str))

# # Perform one-hot encoding on categorical features
# encoder = OneHotEncoder(sparse=False)
# cluster_encoded = encoder.fit_transform(cluster_features)
# cluster_encoded_cols = encoder.get_feature_names_out(cluster_features.columns)
# cluster_encoded_df = pd.DataFrame(cluster_encoded, columns=cluster_encoded_cols)

# Perform clustering using K-means algorithm
kmeans = KMeans(n_clusters=10)  # Adjust the number of clusters as per your data
#cluster_labels = kmeans.fit_predict(cluster_encoded_df)
cluster_labels = kmeans.fit_predict(data)

# Step 4: Assign Cluster Labels
data['cluster_label'] = cluster_labels

# Step 5: Feature Encoding
# One-Hot Encoding of cluster labels
encoder = OneHotEncoder(sparse=False)
cluster_encoded = encoder.fit_transform(data[['cluster_label']])
cluster_encoded_cols = ['cluster_' + str(i) for i in range(cluster_encoded.shape[1])]
cluster_encoded_df = pd.DataFrame(cluster_encoded, columns=cluster_encoded_cols)

# Step 6: Combine with Existing Features
data_combined = pd.concat([data, cluster_encoded_df], axis=1)


# Split the data into training and testing sets
X = data_combined.drop('Accident_severity', axis=1)
y = data_combined['Accident_severity']

# Oversampling
sm = SMOTE(random_state=0)
X_over, y_over = sm.fit_resample(X, y)

# Train-test split
X_train, X_test, y_train, y_test = train_test_split(X_over, y_over, test_size=0.2, random_state=42)

#kxgb
# Hyperparameter Tuning using GridSearchCV for XGBoost
param_grid_kxgb = {
    'booster': ['gbtree', 'dart'],
    'max_depth': [5, 10, 15],
    'learning_rate': [0.01, 0.1, 0.5],
    'n_estimators': [100, 200, 300, 400],
    'objective': ['multi:softmax', 'multi:softprob'],
    'random_state': [42],
    
}

# Perform GridSearchCV with k-fold cross-validation
grid_search_kxgb = GridSearchCV(XGBClassifier(), param_grid_kxgb, cv=5, scoring='accuracy', n_jobs=-1)
grid_search_kxgb.fit(X_train, y_train)

# Get the best hyperparameters and the corresponding model
best_params_kxgb = grid_search_kxgb.best_params_
best_model_kxgb = grid_search_kxgb.best_estimator_

# Train the best model on the full training set
best_model_kxgb.fit(X_train, y_train)

# Predict on the testing set
y_pred_kxgb = best_model_kxgb.predict(X_test)

import matplotlib.pyplot as plt
# Calculate AUC-ROC score
y_pred_prob_kxgb = best_model_kxgb.predict_proba(X_test)
auc_score_xgb = roc_auc_score(y_test, y_pred_prob_kxgb, multi_class='ovr')


# Plot the AUC-ROC curve
fpr = {}
tpr = {}
roc_auc = {}
n_classes = len(np.unique(y_test))
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(y_test, y_pred_prob_kxgb[:, i], pos_label=i)
    roc_auc[i] = auc(fpr[i], tpr[i])

# Plot the ROC curve for each class
plt.figure()
colors = ['blue', 'red', 'green']  # Add more colors if you have more classes
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=2, label='Class {0} (AUC = {1:0.2f})'.format(i, roc_auc[i]))

# Plot the random guessing line
plt.plot([0, 1], [0, 1], color='gray', lw=2, linestyle='--')

# Set the labels and title
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend(loc='lower right')

# Show the plot
plt.show()

# Print the AUC-ROC score
print("AUC-ROC Score:", auc_score_xgb)


# Evaluate the model
print("XGBoost:")
print("Confusion Matrix:")
print(confusion_matrix(y_test, y_pred_kxgb))
print()
print("Classification Report:")
print(classification_report(y_test, y_pred_kxgb))
print("Accuracy:", accuracy_score(y_test, y_pred_kxgb))
print("Recall:", recall_score(y_test, y_pred_kxgb, average='macro'))
print("Precision:", precision_score(y_test, y_pred_kxgb, average='macro'))
print("F1 score:", f1_score(y_test, y_pred_kxgb, average='macro'))
print()

# Perform cross-validation for the best model
cv_scores_kxgb = cross_val_score(best_model_kxgb, X_train, y_train, cv=5, scoring='accuracy', n_jobs=-1)
print(f"Cross-Validation Accuracy (XGBoost): {cv_scores_kxgb}\n")
print(f"Cross-Validation Accuracy (XGBoost): {cv_scores_kxgb.mean():.4f}\n")

import lightgbm as lgb
# Hyperparameter Tuning using GridSearchCV for LightGBM
param_grid_lgb = {
    'boosting_type': ['gbdt', 'dart'],
    'num_leaves': [20, 30, 40],
    'max_depth': [5, 10, 15],
    'learning_rate': [0.01, 0.1, 0.5],
    'n_estimators': [100, 200, 300, 400, 500],
    'objective': ['multiclass'],
    'random_state': [42],
}

# Perform GridSearchCV with k-fold cross-validation
grid_search_lgb = GridSearchCV(lgb.LGBMClassifier(), param_grid_lgb, cv=5, scoring='accuracy')
grid_search_lgb.fit(X_train, y_train)

# Get the best hyperparameters and the corresponding model
best_params_lgb = grid_search_lgb.best_params_
best_model_lgb = grid_search_lgb.best_estimator_

# Train the best model on the full training set
best_model_lgb.fit(X_train, y_train)

# Predict on the testing set
y_pred_lgb = best_model_lgb.predict(X_test)
y_pred_prob_lgb = best_model_lgb.predict_proba(X_test)


# Calculate AUC-ROC score
auc_score_lgb = roc_auc_score(y_test, y_pred_prob_lgb, multi_class='ovr')

# Plot the AUC-ROC curve
fpr = {}
tpr = {}
roc_auc = {}
n_classes = len(np.unique(y_test))
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(y_test, y_pred_prob_lgb[:, i], pos_label=i)
    roc_auc[i] = auc(fpr[i], tpr[i])

# Plot the ROC curve for each class
plt.figure()
colors = ['blue', 'red', 'green']  # Add more colors if you have more classes
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=2, label='Class {0} (AUC = {1:0.2f})'.format(i, roc_auc[i]))

# Plot the random guessing line
plt.plot([0, 1], [0, 1], color='gray', lw=2, linestyle='--')

# Set the labels and title
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend(loc='lower right')

# Show the plot
plt.show()

# Print the AUC-ROC score
print("AUC-ROC Score:", auc_score_lgb)

# Evaluate the model
print("LightGBM:")
print("Confusion Matrix:")
print(confusion_matrix(y_test, y_pred_lgb))
print()
print("Classification Report:")
print(classification_report(y_test, y_pred_lgb))
print("Accuracy:", accuracy_score(y_test, y_pred_lgb))
print("Recall:", recall_score(y_test, y_pred_lgb, average='macro'))
print("Precision:", precision_score(y_test, y_pred_lgb, average='macro'))
print("F1 score:", f1_score(y_test, y_pred_lgb, average='macro'))
print()

# Perform cross-validation for the best model
cv_scores_lgb = cross_val_score(best_model_lgb, X_train, y_train, cv=5, scoring='accuracy')
print(f"Cross-Validation Accuracy (LightGBM): {cv_scores_lgb}\n")
print(f"Cross-Validation Accuracy (LightGBM): {cv_scores_lgb.mean():.4f}\n")



#### Hyperparameter Tuning using GridSearchCV for RandomForestClassifier
import warnings
warnings.filterwarnings('ignore')

param_grid_rf = {
    'n_estimators': [100, 200, 300, 400],
    'criterion': ['gini', 'entropy'],
    'max_depth': [5, 10, 15, 20, 25],
    'min_samples_leaf': [1, 2, 4, 10],
    'min_samples_split': [2, 5, 10, 15],
    'max_features': ['auto', 'log2', 'sqrt'],
}

# Perform GridSearchCV with k-fold cross-validation
grid_search_rf = GridSearchCV(RandomForestClassifier(), param_grid_rf, cv=5, scoring='accuracy')
grid_search_rf.fit(X_train, y_train)

# Get the best hyperparameters and the corresponding model
best_params_rf = grid_search_rf.best_params_
best_model_rf = grid_search_rf.best_estimator_

# Train the best model on the full training set
best_model_rf.fit(X_train, y_train)

# Predict on the testing set
y_pred_rf = best_model_rf.predict(X_test)
y_pred_prob_rf = best_model_rf.predict_proba(X_test)



# Calculate AUC-ROC score
auc_score_rf = roc_auc_score(y_test, y_pred_prob_rf, multi_class='ovr')

# Plot the AUC-ROC curve
fpr = {}
tpr = {}
roc_auc = {}
n_classes = len(np.unique(y_test))
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(y_test, y_pred_prob_rf[:, i], pos_label=i)
    roc_auc[i] = auc(fpr[i], tpr[i])

# Plot the ROC curve for each class
plt.figure()
colors = ['blue', 'red', 'green']  # Add more colors if you have more classes
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=2, label='Class {0} (AUC = {1:0.2f})'.format(i, roc_auc[i]))

# Plot the random guessing line
plt.plot([0, 1], [0, 1], color='gray', lw=2, linestyle='--')

# Set the labels and title
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend(loc='lower right')

# Show the plot
plt.show()

# Print the AUC-ROC score
print("AUC-ROC Score:", auc_score_rf)


# Evaluate the model
print("Random Forest:")
print("Confusion Matrix:")
print(confusion_matrix(y_test, y_pred_rf))
print()
print("Classification Report:")
print(classification_report(y_test, y_pred_rf))
print("Accuracy:", accuracy_score(y_test, y_pred_rf))
print("Recall:", recall_score(y_test, y_pred_rf, average='macro'))
print("Precision:", precision_score(y_test, y_pred_rf, average='macro'))
print("F1 score:", f1_score(y_test, y_pred_rf, average='macro'))
print()

# Perform cross-validation for the best model
cv_scores_rf = cross_val_score(best_model_rf, X_train, y_train, cv=5, scoring='accuracy')
print(f"Cross-Validation Accuracy (Random Forest): {cv_scores_rf}\n")
print(f"Cross-Validation Accuracy (Random Forest): {cv_scores_rf.mean():.4f}\n")
